package com.yyl.wifi_manage;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.yyl.wifi_manage.event.OnConnectDeviceCallback;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * Created by Administrator on 2017/3/16.
 */
public class DeviceAPConnector {

    private String ssidStr;
    private String keyStr;
    private String serverId;
    private boolean isManualDirective;
    private Handler mHandler;
    private DatagramSocket socket;
    private Thread mThread;
    private boolean loop = true;
    private String customStr;
    private int retry = 0;
    private int maxRetry = 3;
    private String backStr;

    public DeviceAPConnector(Context context) {
        mHandler = new Handler(context.getMainLooper());
    }



    public void setSsidAndKeyStr(String ssidStr, String keyStr, String serverId,boolean isManualDirective) {
        this.ssidStr = ssidStr;
        this.keyStr = keyStr;
        this.serverId = serverId;
        this.isManualDirective = isManualDirective;
    }

    public void initSocket() {
        Log.d("DeviceAPConnector", "initSocket socket before:" + socket);
        try {
            if (socket == null) {
                int port = 8800;
                socket = new DatagramSocket(null);
                socket.setReuseAddress(true);
                socket.setBroadcast(true);
                socket.bind(new InetSocketAddress(port));
                loop = true;
            }
            startRevice();
        } catch (Exception e) {
            e.printStackTrace();
            closeSocket();
        }
        Log.d("DeviceAPConnector", "initSocket socket after:" + socket);
    }

    private void closeSocket() {
        Log.d("DeviceAPConnector", "closeSocket");
        loop = false;
        try {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            socket = null;
        }
        try {
            if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            mThread = null;
        }
    }

    public void start() {
        Log.d("DeviceAPConnector", "start");
        retry = 0;
        initSocket();
        startConnect();
    }

    private void startRevice() {
        if (mThread == null) {
            mThread = new Thread() {
                @Override
                public void run() {
                    while (loop) {
                        byte receiveData[] = new byte[1024];
                        try {
                            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            socket.receive(receivePacket);
                            final String receiveDataStr = new String(receiveData, receivePacket.getOffset(), receivePacket.getLength(), "UTF-8");
                            Log.d("APConnectFragment", "receiveDataStr :" + receiveDataStr);
                            mHandler.removeMessages(0);
                            checkResult(receiveDataStr);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            };
            mThread.start();
        }
    }

    private final int STATE_CONNECT = 0;
    private final int STATE_SET_SSID = 1;
    private final int STATE_SET_KEY = 2;
    private final int STATE_SET_CUSTOM_ONE = 3;
    private final int STATE_SET_CUSTOM_TWO = 4;
    private int state = STATE_CONNECT;

    private void addTimeout() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callbackNotResponse();
            }
        }, 10000);
    }

    private void startConnect() {
        Log.d("DeviceAPConnector", "startConnect");
        addTimeout();
        state = STATE_CONNECT;
        sendData("LSD_WIFI");
    }

    private void setDeviceSSid() {
        addTimeout();
        state = STATE_SET_SSID;
        sendData(ssidStr);
    }

    private void setDeviceKey() {
        addTimeout();
        state = STATE_SET_KEY;
        sendData(keyStr);
    }

    public void setCustomOne(String custom) {
        if(TextUtils.isEmpty(custom)){
            return;
        }
        addTimeout();
        state = STATE_SET_CUSTOM_ONE;
        customStr = custom;
        sendData("LSD_WIFI:"+customStr + "\r\n");
    }

    public void setCustomTwo(String custom) {
        addTimeout();
        state = STATE_SET_CUSTOM_TWO;
        sendData("LSD_WIFI:"+custom + "\r\n");
    }

    private void performCallbackFail() {
        closeSocket();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onFail();
                }
            }
        });
    }


    private void callbackNotResponse() {
        if (retry < maxRetry) {
            retry++;
            Log.d("DeviceAPConnector", "callbackNotResponse retry :" + retry);
            closeSocket();
            initSocket();
            if (state == STATE_SET_CUSTOM_ONE || state == STATE_SET_CUSTOM_TWO) {
                setCustomOne(customStr);
                return;
            }
            startConnect();
        } else {
            performCllbackNotResponse();
        }
    }

    private void performCllbackNotResponse() {
        closeSocket();
        if (callback != null) {
            if(state == STATE_SET_CUSTOM_ONE || state == STATE_SET_CUSTOM_TWO){
                callback.onNotResponse(false);
                return;
            }
            callback.onNotResponse(true);
        }
    }

    private void callbackFail() {
        if (retry < maxRetry) {
            retry++;
            closeSocket();
            initSocket();
            if (state == STATE_SET_CUSTOM_ONE || state == STATE_SET_CUSTOM_TWO) {
                setCustomOne(customStr);
                return;
            }
            startConnect();
        } else {
            performCallbackFail();
        }
    }

    private void callbackSuccess() {
        if(!isManualDirective) {
            closeSocket();
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    if (isManualDirective) {
                        callback.onBeginCustomSend(backStr);
                        return;
                    }
                    callback.onSuccess(backStr);
                }
            }
        });
    }

    private String bindDevice;

    private void checkResult(String receiveDataStr) {
        String orderStr=null;
        switch (state) {
            case STATE_CONNECT:
                if (!TextUtils.isEmpty(receiveDataStr)) {
                    String ss[] = receiveDataStr.split(",");
                    if (ss.length == 2) {
                        bindDevice = ss[0];
                        setDeviceSSid();
                        orderStr="mac:"+bindDevice;
                    } else {
                        callbackFail();
                    }
                } else {
                    callbackFail();
                }
                break;
            case STATE_SET_SSID:
                orderStr="SSID指令返回:"+receiveDataStr;
                if (receiveDataStr.contains("+ok")) {
                    setDeviceKey();
                } else {
                    callbackFail();
                }
                break;
            case STATE_SET_KEY:
                orderStr="SSID_KEY指令返回:"+receiveDataStr;
                if (receiveDataStr.contains("+ok")) {
                    callbackSuccess();
                } else {
                    callbackFail();
                }
                break;
            case STATE_SET_CUSTOM_ONE:
                orderStr="设置服务器地址指令返回:"+receiveDataStr;
                if (receiveDataStr.contains("+ok")) {
                    setCustomTwo("AT+NETP");
                } else {
                    callbackFail();
                }
                break;
            case STATE_SET_CUSTOM_TWO:
                orderStr="AT+NETP指令返回:"+receiveDataStr;
                String[]  str=receiveDataStr.split("=");
                String[] lastStr=customStr.split("=");
                String order= str[1].replace("\r\n\r\n", "").trim();
                if (str.length!=0 && order.equals(lastStr[1].toUpperCase())) {
                    isManualDirective = false;
                    bindDevice=receiveDataStr;
                    callbackSuccess();
                } else {
                    callbackFail();
                }
                break;
        }
        callback.orderBack(orderStr);

    }

    private void sendData(final String text) {
        new Thread() {
            @Override
            public void run() {
                try {
                    byte[] data = text.getBytes("UTF-8");
                    InetAddress serverAddress = InetAddress.getByName(serverId);
                    int port = 8800;
                    DatagramPacket packet = new DatagramPacket(data, data.length, serverAddress, port);
                    socket.send(packet);
                    Log.d("DeviceAPConnector", "send");
                } catch (Exception e) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    callbackFail();
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private OnConnectDeviceCallback callback;

    public void setOnConnectCallback(OnConnectDeviceCallback callback) {
        this.callback = callback;
    }


}

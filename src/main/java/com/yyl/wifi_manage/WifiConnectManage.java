package com.yyl.wifi_manage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;


import com.yyl.wifi_manage.bean.WifiBaseInfo;
import com.yyl.wifi_manage.event.OnConnectDeviceCallback;
import com.yyl.wifi_manage.event.WifiManageEvent;
import com.yyl.wifi_manage.event.WifiManageStatus;
import com.yyl.wifi_manage.until.netUtil;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import static com.yyl.wifi_manage.until.netUtil.intToInetAddress;
import static com.yyl.wifi_manage.until.netUtil.intToIp;


/**
 * Created by 90835 on 2017/12/1.
 */

public class WifiConnectManage implements WifiManageEvent, OnConnectDeviceCallback {

    public static final int CONNECT_FAIL = 0;
    public static final int CONNECT_SUCCESSS = 1;
    public static final int NOT_DEVICE = 2;
    public static final int PASSWORD_ERROR = 3;
    public static final int NOT_RESPONSE = 4;
    public static final int SETTING_FAIL = 5;

    public static final String AUTH_OPEN = "OPEN";
    public static final String AUTH_WPAPSK = "WPAPSK";
    public static final String AUTH_WPA2PSK = "WPA2PSK";

    public static final String ENCRY_NONE = "NONE";
    public static final String ENCRY_AES = "AES";
    private String defaultDeviceSSID = "LivingSmart";

    private WifiAdmin wifiAdmin;
    private Context mContext;
    private WifiManageStatus listener;
    private DeviceAPConnector connector;
    private String ssidStr;
    private String keyStr;
    private WifiBaseInfo info;
    private long lastTime;
    private boolean isConnect;
    private ScanResult connectNetWorkWifi;
    private ScanResult connectDeviceWifi;
    private String wifiPwd;
    private Observable connectObserVable;
    private Subscription addTimeOut;
    private int retryCount = 0;
    //是否连接设备后手动输入指令，默认false
    private boolean  isManualDirective;


    public WifiConnectManage(Context mContext, WifiManageStatus listener) {
        this.mContext = mContext;
        this.listener = listener;
        wifiAdmin = new WifiAdmin(mContext);
        connector = new DeviceAPConnector(mContext);
    }

    public void setManualDirective(boolean manualDirective) {
        isManualDirective = manualDirective;
    }

    public WifiInfo getCurrentWifiInfo() {
        return wifiAdmin.getConnectionInfo();
    }


    /*
    * 获取扫描列表
    * */
    @Override
    public Observable<List<ScanResult>> getScanResult() {
        return Observable.just(wifiAdmin)
                .subscribeOn(Schedulers.newThread())
                .map(new Func1<WifiAdmin, Boolean>() {
                    @Override
                    public Boolean call(WifiAdmin wifiAdmin) {
                        if (wifiAdmin.isWifiEnabled()) {
                            wifiAdmin.startScan();
                        } else {
                            boolean opened = wifiAdmin.openWifi();
                            if (opened) {
                                wifiAdmin.startScan();
                            } else {
                                return false;
                            }
                        }
                        return true;
                    }
                }).map(new Func1<Boolean, List<ScanResult>>() {
                    @Override
                    public List<ScanResult> call(Boolean aBoolean) {
                        if (!aBoolean) {
                            return null;
                        }
                        return wifiAdmin.getWifiList();
                    }
                }).observeOn(AndroidSchedulers.mainThread());
    }

    /*
    * 根据SSID获取指定的wifi对象
    * */
    @Override
    public Observable<ScanResult> getFilterWifi(final String ssid) {
        return Observable.just(wifiAdmin.getScanResults())
                .map(new Func1<List<ScanResult>, ScanResult>() {
                    @Override
                    public ScanResult call(List<ScanResult> scanResults) {
                        for (int i = 0; i < scanResults.size(); i++) {
                            if (scanResults.get(i).SSID.equals(ssid)) {
                                return scanResults.get(i);
                            }
                        }
                        return null;
                    }
                }).filter(new Func1<ScanResult, Boolean>() {
                    @Override
                    public Boolean call(ScanResult scanResult) {
                        return scanResult != null;
                    }
                });
    }


    //连接路由wifi
    @Override
    public Observable<Boolean> startConnect(final ScanResult result, final String pwd) {
        connectObserVable = Observable.zip(
                getConnectAddNetWork(result, pwd),
                getConnectStatus(), new Func2<Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean, Boolean aBoolean2) {
                        if (aBoolean2) {
                            unregReceiver();
                            isConnect = false;
                            WifiInfo wifiInfo = wifiAdmin.getConnectionInfo();
                            if (wifiInfo != null) {
                                connectNetWorkWifi = result;
                                wifiPwd = pwd;
                                ssidStr = "LSD_WIFI:AT+WSSSID=" +connectNetWorkWifi.SSID+ "\r";
                                keyStr = netUtil.joinKeyStr(info, wifiPwd);
                            }
                        }
                        return aBoolean && aBoolean2;
                    }
                });
        return connectObserVable;
    }


    //根据deviceSsid来筛选设备wifi,如果为空则使用默认值LivingSmart
    public void initConnectDeviceWifi(String deviceSsid) {
        if (!TextUtils.isEmpty(deviceSsid)) {
            defaultDeviceSSID = deviceSsid;
        }
        String wifiPwd = "";
        getFilterWifi(defaultDeviceSSID).subscribe(new Action1<ScanResult>() {
            @Override
            public void call(ScanResult scanResult) {
                connectDeviceWifi = scanResult;
            }
        });
        if (connectDeviceWifi == null) {
            listener.operationError(NOT_DEVICE);
            return;
        }
        beginConnectDeviceWifi();
    }

    public void beginConnectDeviceWifi() {
        wifiAdmin.clearAllNetwork();
        addTimeOut();
        Observable.zip(
                getConnectAddNetWork(connectDeviceWifi, null),
                getConnectStatus(), new Func2<Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean, Boolean aBoolean2) {
                        return aBoolean && aBoolean2;
                    }
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        if (aBoolean && addTimeOut != null) {
                            cleanTimeOut();
                            unregReceiver();
                            wifiAdmin.creatWifiLock();
                            wifiAdmin.acquireWifiLock();
                            connect();
                        }
                    }
                });
    }

    //添加网络wifi
    private Observable<Boolean> getConnectAddNetWork(final ScanResult result, final String wifiPwd) {
        return Observable.just(result)
                .map(new Func1<ScanResult, Boolean>() {
                    @Override
                    public Boolean call(ScanResult scanResult) {
                        WifiConfiguration wifiConfiguration = wifiAdmin.createWifiInfo(result, wifiPwd);
                        return wifiAdmin.addNetwork(wifiConfiguration);
                    }
                })
                .subscribeOn(Schedulers.io())
                .all(new Func1<Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean) {
                        if (aBoolean) {
                            mContext.registerReceiver(receiver, new IntentFilter(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION));
                        }
                        return aBoolean;
                    }
                });
    }

    //轮询获取连接状态，当isConnect为True则连接成功
    private Observable<Boolean> getConnectStatus() {
        return Observable.interval(1, TimeUnit.SECONDS)
                .map(new Func1<Long, Boolean>() {
                    @Override
                    public Boolean call(Long aLong) {
                        return isConnect;
                    }
                }).subscribeOn(Schedulers.io()).filter(new Func1<Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean) {
                        return aBoolean;
                    }
                });
    }


    //添加超时事件
    private void addTimeOut() {
        addTimeOut = Observable.timer(15, TimeUnit.SECONDS)
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (retryCount > 2) {
                            listener.operationError(CONNECT_FAIL);
                            retryCount = 0;
                            return;
                        }
                        retrySend();
                    }
                });
    }

    //清除超时事件
    private void cleanTimeOut() {
        if (addTimeOut != null) {
            addTimeOut.unsubscribe();
            addTimeOut = null;
        }
    }

    //重试发送信息给设备
    private void retrySend() {
        retryCount++;
        cleanTimeOut();
        beginConnectDeviceWifi();
    }

    //注销广播
    private void unregReceiver() {
        try {
            mContext.unregisterReceiver(receiver);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
                // Ignore this exception. This is exactly what is desired
            } else {
                // unexpected, re-throw
                throw e;
            }
        }
    }

    //开始UDP连接，serverId为设备IP地址，
    private void connect() {
        DhcpInfo dhcpInfo = wifiAdmin.getDchcpInfo();
        String serverId = intToInetAddress(dhcpInfo.serverAddress).getHostAddress();
        connector.setOnConnectCallback(this);
        connector.setSsidAndKeyStr(ssidStr, keyStr, serverId,isManualDirective);
        Observable.timer(2000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        connector.start();
                    }
                });
    }

    /*
    *
    * 判断wifi对象加密方式
    * */
    @Override
    public Observable<WifiBaseInfo> wifiIs(ScanResult result) {
        return Observable.just(result)
                .map(new Func1<ScanResult, WifiBaseInfo>() {
                    @Override
                    public WifiBaseInfo call(ScanResult result) {
                        String cap = result.capabilities;
                        info = new WifiBaseInfo();
                        if (cap.contains("WPA2-PSK-CCMP") || cap.contains("WPA2-PSK-CCMP+TKIP")) {
                            info.setEncryMode(AUTH_WPA2PSK);
                            info.setHasPassword(true);
                        } else if (cap.contains("WPA-PSK-CCMP") || cap.contains("WPA-PSK-CCMP+TKIP")) {
                            info.setEncryMode(AUTH_WPAPSK);
                            info.setHasPassword(true);
                        } else {
                            info.setEncryMode(AUTH_OPEN);
                            info.setHasPassword(false);
                        }
                        return info;
                    }
                });
    }

    @Override
    public void sendDirective(String sendStr) {
        if(connector!=null){
            connector.setCustomOne(sendStr);
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {
                Log.d("MainActivity", "wifi连接广播");
                SupplicantState parcelable = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
                int linkWifiResult = intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, 123);
                Log.d("MainActivity", "parcelable :" + parcelable + ",linkWifiResult :" + linkWifiResult);
                if (linkWifiResult == WifiManager.ERROR_AUTHENTICATING && parcelable == SupplicantState.DISCONNECTED) {
                    Log.d("MainActivity", "密码错误");
                    connectObserVable.unsubscribeOn(Schedulers.io());
                    wifiAdmin.clearAllNetwork();
                    listener.operationError(PASSWORD_ERROR);
                } else if (parcelable == SupplicantState.COMPLETED) {
                    if (System.currentTimeMillis() - lastTime < 2000) {
                        return;
                    }
                    lastTime = System.currentTimeMillis();
                    Log.d("MainActivity", "连接成功");
                    isConnect = true;
                }
            }
        }
    };

    @Override
    public void onSuccess(final String bindDevice) {
        retryCount = 0;
        wifiAdmin.releaseWifiLock();
        wifiAdmin.clearAllNetwork();
        wifiAdmin.addNetwork(wifiAdmin.createWifiInfo(connectNetWorkWifi, wifiPwd));
        Observable.just(bindDevice)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        listener.deviceBindSuccess(bindDevice);
                    }
                });

    }

    @Override
    public void onFail() {
        wifiAdmin.releaseWifiLock();
        listener.operationError(SETTING_FAIL);
    }

    @Override
    public void onNotResponse(boolean isRety) {
        if (retryCount > 2 || !isRety) {
            listener.operationError(NOT_RESPONSE);
            retryCount=0;
        }
        retrySend();
    }

    @Override
    public void onBeginCustomSend(String bindDevice) {
        listener.deviceBindSuccess(null);
    }

    @Override
    public void orderBack(String order) {
        listener.orderBack(order);
    }
}

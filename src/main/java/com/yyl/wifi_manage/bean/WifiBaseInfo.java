package com.yyl.wifi_manage.bean;

/**
 * Created by 90835 on 2017/12/1.
 */

public class WifiBaseInfo {

    private String encryMode;

    private boolean hasPassword;

    public String getEncryMode() {
        return encryMode;
    }

    public void setEncryMode(String encryMode) {
        this.encryMode = encryMode;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }
}

package com.yyl.wifi_manage.event;

/**
 * Created by 40503 on 2017/12/22.
 */

public interface OnConnectDeviceCallback {
    void onSuccess(String bindDevice);

    void onFail();

    void onNotResponse(boolean isRety);

    void onBeginCustomSend(String bindDevice);

    void orderBack(String order);

}

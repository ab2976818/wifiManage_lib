package com.yyl.wifi_manage.event;

import android.net.wifi.ScanResult;


import com.yyl.wifi_manage.bean.WifiBaseInfo;

import java.util.List;

import rx.Observable;

/**
 * Created by 90835 on 2017/12/1.
 */

public interface WifiManageEvent {

    Observable<List<ScanResult>> getScanResult();

    Observable<ScanResult> getFilterWifi(String ssid);

    Observable startConnect(ScanResult result, String pwdStr);

     void initConnectDeviceWifi(String ssid);

     void beginConnectDeviceWifi();

    Observable<WifiBaseInfo> wifiIs(ScanResult result);

    void  sendDirective(String sendStr);


}

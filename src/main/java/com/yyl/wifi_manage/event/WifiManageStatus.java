package com.yyl.wifi_manage.event;

import android.view.View;

/**
 * Created by 90835 on 2017/12/1.
 */

public interface WifiManageStatus {

    void operationError(int errorCode);

    void deviceBindSuccess(String bindDevice);

    void orderBack(String order);
}

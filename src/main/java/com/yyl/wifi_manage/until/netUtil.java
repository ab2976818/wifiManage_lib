package com.yyl.wifi_manage.until;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import com.yyl.wifi_manage.bean.WifiBaseInfo;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static com.yyl.wifi_manage.WifiConnectManage.AUTH_OPEN;
import static com.yyl.wifi_manage.WifiConnectManage.ENCRY_AES;
import static com.yyl.wifi_manage.WifiConnectManage.ENCRY_NONE;


public class netUtil {

    public static String intToIp(int i) {
        return (i & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                (i >> 24 & 0xFF);
    }

    public static InetAddress intToInetAddress(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};
        try {
            return InetAddress.getByAddress(addressBytes);
        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }

    public static String joinKeyStr(WifiBaseInfo info,String wifiPwd) {
        String type;
        if (info == null) {
            type = AUTH_OPEN;
        } else {
            type = info.getEncryMode();
        }
       return  "LSD_WIFI:AT+WSKEY=" + type + "," +
                (TextUtils.isEmpty(wifiPwd) ? ENCRY_NONE : ENCRY_AES) + "," + wifiPwd + "\r";
    }
}
